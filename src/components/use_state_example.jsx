import React, { useState } from 'react'

export default function UseStateExample() {

const [counter, setCounter] = useState(0);
const [a, setA] = useState(0);
const [b, setB] = useState(0);
const [result, setResult] = useState(0);
// const [getter, setter] = useState(<default value>)

const sumAB = () => {
    console.log('a ', a);
    console.log('b ', b);
    console.log(Number(a) + Number(b));
    setResult(Number(a) + Number(b));
}

  return (
    <div style={{ marginTop: 100 }}>
        UseStateExample
        <div>
            counter is { counter }
        </div>
        <div>
            <button onClick={() => setCounter(previousValue => previousValue + 1)}>
                Add
            </button>
            <button onClick={() => setCounter(previousValue => previousValue - 1)}>
                Del
            </button>
        </div>
        <div>
            A :
            <input 
                type='number' 
                value={a}
                onInput={(e) => setA(e.target.value)} 
            />
            <div>
                { a }
            </div>
        </div>
        <div>
            B :
            <input type='number' value={b} onInput={(e) => setB(e.target.value)} />
            <div>
                { b }
            </div>
        </div>
        <div>
            <button onClick={sumAB}>
                Sum
            </button>
        </div>
        <div>
            { result }
        </div>
    </div>
  )
}
