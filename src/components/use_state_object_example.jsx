import './use_state_object_example.css'
import React, { useState } from 'react'

export default function UseStateObject() {
    const intialState = { name: "", weight: 0, height: 0 };
    const [animal, setAnimal] = useState(intialState);

    // initArray = []
    // const [arr, setArr] = useState(initArray);
    
    // setArr([...arr, newArray]) 
    // or const old = arr; const newArray old.push(newArr); setArr(newArray);

    //  name: "", weight: 0, height: 0 
    // {}
    // {...animal} ^
    
    function saveAnimal() {
        alert('Hi');
    }

  return (
    <div>
        <div className='container2'>Animal Form</div>
        <div className='container2'>
            Name : <input onChange={(e) => setAnimal({...animal, name: e.target.value})} />
        </div>
        <div className='container2'>
            Weight : <input type='number' onChange={(e) => setAnimal({...animal, weight: e.target.value})} />
        </div>
        <div className='container2'>
            Height : <input type='number' onChange={(e) => setAnimal({...animal, height: e.target.value})} />
        </div>
        <div className='container2'>
            { JSON.stringify(animal) }
        </div>
        <div className='container2'>
            <button onClick={saveAnimal}>Save Animal</button>
        </div>
        
    </div>
  )
}
