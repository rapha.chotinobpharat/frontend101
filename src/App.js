import './App.css';
// import ClassComponent from './components/class_component';
// import FunctionComponent from './components/function_component';
// import UseStateExample from './components/use_state_example';
// import UseStateObject from './components/use_state_object_example';
// import UseEffectExample from './components/use_effect';
import Animal from './components/animal'

function App() {
  return (
    <div className='container'>
      {/* <ClassComponent></ClassComponent> */}
      {/* <FunctionComponent/> */}
      {/* <UseStateExample/> */}
      {/* <UseStateObject/> */}
      {/* <UseEffectExample/> */}
      <Animal/>
    </div>
  );
}

export default App;
